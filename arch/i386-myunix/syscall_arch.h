static inline long __syscall0(long n)
{
    unsigned long __ret;
    __asm__ __volatile__("int $0x80" : "=a"(__ret) : "a"(n) : "memory");
    return __ret;
}

static inline long __syscall1(long n, long a1)
{
    unsigned long __ret;
    __asm__ __volatile__("int $0x80" : "=a"(__ret) : "a"(n), "b"(a1) : "memory");
    return __ret;
}
