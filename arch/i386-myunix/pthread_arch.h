static inline struct pthread *__pthread_self()
{
    __asm__ __volatile__ ("mov %eax, 0x3c\n"
                  "int $128");
}

#define TP_ADJ(p) (p)

#define MC_PC gregs[REG_EIP]
