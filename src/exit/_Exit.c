#include <stdlib.h>
#include "syscall.h"

_Noreturn void _Exit(int ec)
{
#ifndef __myunix__
	__syscall(SYS_exit_group, ec);
#endif
	for (;;) __syscall(SYS_exit, ec);
}
